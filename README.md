# Laravel Aimon Package

A laravel wrapper package for the Aimon.it API.

For more information see [Aimon](https://www.aimon.it/)

## Requirements

Laravel 6 or later

## Installation

Installation is a quick 1 step process:

1. Download aimon-laravel using composer

### Step 1: Configure Aimon credentials

```
php artisan vendor:publish --provider="Dmgroup\Aimon\AimonServiceProvider"
```

Add this in you **.env** file

```
AIMON_SMS_LOGIN=your_login
AIMON_SMS_PASSWD=your_password
AIMON_ID_API=your_id_api
AIMON_DATABASE_LOG=true // set false if database log is not required
```

## Usage

```php
<?php

$sms = app('aimon')->sendSms([
        'to' => '391234567891', // mobile including country code without +
        'from' => 'Workgroup',
        'text' => 'Hello world',
    ]); // the second parameter is optional if database log is disabled
```

## Documentation

http://sms.aimon.it/documentazione_api/Documentazione_BCP_API.pdf
